# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# source bash completions
if [ -f /usr/share/bash-completion/completions/git ]; then
  . /usr/share/bash-completion/completions/git
fi

# User specific aliases and functions

export NODE_ENV=development
export DEBUG=true

export LOGLEVEL=debug

export PATH="$PATH:~/node_modules/.bin"

alias cdm="cd ~/middleware/modules"
alias cdc="cd ~/middleware-consumer/modules"
alias rd="clear;DEBUG=1 node ~/dabble-app-server/modules/server.js"
alias ts="ssh -i ~/important/gideonMWJ.ppk gideonj@34.245.83.47"
alias nrl="npm run lint"
alias rd="redis-cli -h dabble.bsdjej.clustercfg.euw1.cache.amazonaws.com -c"

alias medis="~/medis/run.sh"

. ~/.bash_paranoia

function req {
  curl -s $1 | python -mjson.tool
}

function setup {
  # setup vim plugin manager
  curl -fLo ~/.vim/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  vim +PlugInstall +qall

  # setup nvm
  curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.9/install.sh | bash

  npm install
}

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

nvm use 9.11.1 2> /dev/null
