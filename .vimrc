set background=dark
colorscheme flattown

set nocp

set smartindent
set tabstop=2
set shiftwidth=2
set expandtab
set autoindent
set number
set showbreak=↪

command WQ wq
command Wq wq
command Q q
command W w
command E e

" strip trailing whitespace
autocmd BufWritePre * :%s/\s\+$//e

" nerdtree preferences
autocmd StdinReadPre * let s:std_in=1
map <C-n> :NERDTreeToggle<CR>

" vim-gitgutter preferences
let g:gitgutter_enabled = 1
let g:gitgutter_realtime = 1
highlight clear SignColumn

" vim-javascript preferences
let g:javascript_plugin_jsdoc = 1

" indentline preferences
let g:indentLine_color_term = 239

call plug#begin()
Plug 'tpope/vim-sensible'

" On-demand loading
Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }

Plug 'airblade/vim-gitgutter'

Plug 'townk/vim-autoclose'

Plug 'bling/vim-airline'

Plug 'pangloss/vim-javascript'

Plug 'yggdroot/indentline'

Plug 'kien/ctrlp.vim'

call plug#end()
